# Jan Kowalski

## O mnie
Cześć, nazywam się Jan Kowalski, jestem początkującym testerem oprogramowania, w tym miejscu chciałbym podzielić się z Tobą moim doświadczeniem oraz projektami, które miałem dotychczas przyjemność wykonać. 
<center>

![profile](img/profile.png)

</center>

## Testowanie Oprogramowania
Testowanie oprogramowania jest niezmiennie ważnym elementem procesu wytwarzania oprogramowania. Moje pierwsze kroki w tym kierunku stawiałem dość nieświadomie korzystając
z różnych aplikacji, w których miałem okazje znaleźć pewne nieścisłości. Jestem osobą, która lubi poszukać dziury w całym, a jednocześnie skupiam się na detalach. Postanowiłem, że zostanę testerem oprogramowania i moje umiejętności przydadzą się w Twoim zespole.

## Kurs Software Development Academy
Miałem przyjemność uczestniczyć w kursie "Tester Manualny" organizowanym przez Software Development Academy. Przez 105 godzin zajęć oraz wiele godzin poświęconych na pracę samodzielną zdobyłem wiedzę z następujacych tematów:

<center>

![profile](img/course_cert.png)

</center>

Sprawnie będę poruszał się także w projektach zwinnych, dzięki zajęciom wprowadzających do metodyki Scrum:

<center>

![profile](img/scrum.png)

</center>

## Certyfikat ISTQB
W czasie kursu, przygotowywaliśmy się do egzaminu ISTQB Foundation Level, który miałem przyjemność zdawać w formie zdalnej.

<center>

![profile](img/istqb_example.png)

</center>

## Moje projekty

* [Przypadki Testowe](src/test_cases1.md)  dla strony [PHPTravels](https://www.phptravels.net/home)

* [Przypadki Testowe](src/test_cases1.md) dla strony [AutomationPractice](http://automationpractice.com/index.php)

* [Zapytania](src/zapytania.md) REST API dla serwisu [Restful-Booker](https://restful-booker.herokuapp.com/)

* [Testy Automatyczne](src/seleniumide.md) nagrane przy pomocy narzędzia Selenium IDE.

* Moje pierwsze [projekty programistyczne](src/programming.md).

* [Testy Automatyczne](src/seleniumwebdriver.md), z wykorzystaniem Selenium WebDriver.

## Technologie

<center>

![profile](img/technical_skills.png)

</center>

## Zainteresowania
Komputery to nie wszystko, w wolnym czasie eksploruje lasy na rowerze górskim.

<center>

![mtb](img/mtb.png)

</center>

## Kontakt
Skontaktuj się ze mną mailowo: tester@google.com
